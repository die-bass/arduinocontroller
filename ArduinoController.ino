// Include the libraries we need
#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into port 4 on the Arduino
#define ONE_WIRE_BUS 4
#define TEMPERATURE_PRECISION 1
#define MAX_BAD_SENSOR_READINGS 5
#define UPDATE_INTERVAL 10000
#define TEMP_MAX 90.0f
#define TEMP_MIN -20.0f

#define NUMBER_SENSORS 4
#define NUMBER_FANS 4
#define NUMBER_STAGES 5
#define DEFAULT_STAGE NUMBER_STAGES

#define MIN_FAN_SPEED 10

// LOG Level 0 = Off, 1 = WARN, 2 = INFO, 3 = DEBUG
#define LOG_OFF 0
#define LOG_WARN 1
#define LOG_INFO 2
#define LOG_DEBUG 3
#define LOG_LEVEL LOG_DEBUG

// ERROR Codes
#define ERROR_BAD_READING 1
#define ERROR_OUT_RANGE 2

#define ERROR_LED_PIN LED_BUILTIN

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensorBus(&oneWire);

typedef struct {
	DeviceAddress address;
	float value;
	String name;
} TempSensor;

TempSensor tempSensors[NUMBER_SENSORS] = {
	{{ 0x28, 0xC8, 0x6F, 0x57, 0x04, 0xE1, 0x3C, 0x32 }, DEVICE_DISCONNECTED_C, "inlet"},
	{{ 0x28, 0xFF, 0x10, 0x32, 0x8A, 0x16 ,0x03, 0xD7 }, DEVICE_DISCONNECTED_C, "amp_h"}, // amp high
	{{ 0x28, 0xFF, 0x24, 0x7E, 0x92, 0x16, 0x04, 0x52 }, DEVICE_DISCONNECTED_C, "amp_l"}, // amp low
	{{ 0x28, 0x0D, 0x56, 0x57, 0x04, 0xE1, 0x3C, 0x31 }, DEVICE_DISCONNECTED_C, "outlet"}
};

typedef struct {
	int pin;
	uint8_t speed;
	String name;
} Fan;

Fan fans[NUMBER_FANS] = {
	{6, 0, "inlet"},
	{45, 0, "amp_a"},  // amp angle
	{44,  0, "amp_s"},  // amp streight
	{46, 0, "outlet"}
};



typedef struct {
	uint8_t fanSpeeds[NUMBER_FANS];
	float maxTemps[NUMBER_SENSORS];
} Stage;

Stage stages[NUMBER_STAGES] = {
	/* ------------------------- Stages Definition -----------------------------
	               Fans                     |     Temperature Sensors
	   inlet,     ampa,amps,        outlet  | inlet,   amph,  ampl, outlet
	---------------------------------------------------------------------------- */
	{{MIN_FAN_SPEED, 0,   0, MIN_FAN_SPEED}, {100.0f, 30.0f, 30.0f, 30.0f}},
	{{20,           10,  10,            20}, {100.0f, 34.5f, 34.5f, 33.5f}},
	{{40,           20,  20,            40}, {100.0f, 50.0f, 50.0f, 35.0f}},
	{{50,           40,  40,            50}, {100.0f, 60.0f, 60.0f, 40.0f}},
	{{80,           50,  50,            80}, {100.0f, 75.0f, 75.0f, 45.0f}}
	//Note: the default stage does not need to be defined
};

int currentStage = DEFAULT_STAGE;

// function to read the temperature for a device
int readTemperature(TempSensor *sensor)
{
	sensor->value = sensorBus.getTempC(sensor->address);

	if (sensor->value == DEVICE_DISCONNECTED_C) {
		#if LOG_LEVEL >= LOG_WARN
			Serial.print("Error: Could not read temperature data for temp sensor: ");
			Serial.println(sensor->name);
		#endif
		return ERROR_BAD_READING;
	}

	if (sensor->value < TEMP_MIN || sensor->value > TEMP_MAX) {
		#if LOG_LEVEL >= LOG_WARN
			Serial.print("Error: Temperature data out of range for temp sensor: ");
			Serial.println(sensor->name);
		#endif
		return ERROR_OUT_RANGE;
	}

	#if LOG_LEVEL >= LOG_DEBUG
		Serial.print(sensor->name);
		Serial.print(": ");
		Serial.println(sensor->value);
	#endif
	return 0;
}

// function to read all sensors and store the value
int readTemperatures() {
	// call sensorBus.requestTemperatures() to issue a global temperature
	// request to all devices on the bus
	#if LOG_LEVEL >= LOG_DEBUG
		Serial.print("Requesting temperatures...");
	#endif
	sensorBus.requestTemperatures();
	#if LOG_LEVEL >= LOG_DEBUG
		Serial.println("done");
	#endif

	for (int i = 0; i < NUMBER_SENSORS; i++) {
		if (readTemperature(&tempSensors[i]))
			return ERROR_BAD_READING;
	}
	return 0;
}

void setStage() {
  #if LOG_LEVEL >= LOG_DEBUG
    Serial.println("Setting Stage:");
  #endif
	int stage = 0;
	bool stage_found = false;

	currentStage = DEFAULT_STAGE;

	while (!stage_found && stage < NUMBER_STAGES) {
		//we assume that the stage matches and try to prove wrong
		stage_found = true;
		for (int sensor = 0; sensor < NUMBER_SENSORS; sensor++) {
			//first check if sensor is in error state
			if (tempSensors[sensor].value == DEVICE_DISCONNECTED_C) {
        #if LOG_LEVEL >= LOG_INFO
          Serial.println("Setting DEFAULT stage! Cause: wrong sensor reading");
        #endif
				//if any sensor is in error state immediately return
				return;
      }
			//check if stage does not match
			if (tempSensors[sensor].value > stages[stage].maxTemps[sensor]) {
				stage_found = false;
				break;
			}
		}
		//if stage is not found, try the next stage
		if (!stage_found)
			stage++;
	}
	if (stage_found) {
		currentStage = stage;
    #if LOG_LEVEL >= LOG_INFO
      Serial.print("Setting stage: ");
      Serial.println(stage+1);
    #endif
  }
  #if LOG_LEVEL >= LOG_INFO
    else
      Serial.println("Setting DEFAULT stage! Cause: no matching stage");
  #endif
}

void setFanSpeeds() {
  #if LOG_LEVEL >= LOG_DEBUG
    Serial.println("Setting Fan Speeds:");
  #endif
	for (int fan_idx = 0; fan_idx < NUMBER_FANS; fan_idx++) {
		// if the default stage has matched, set speed to 100%
		if (currentStage == DEFAULT_STAGE)
			fans[fan_idx].speed = 100;
		else
			fans[fan_idx].speed = stages[currentStage].fanSpeeds[fan_idx];

		// speed is in percent -> needs to be mapped to [0;255]
		analogWrite(fans[fan_idx].pin, (int)(fans[fan_idx].speed * 255 / 100));
    #if LOG_LEVEL >= LOG_DEBUG
      Serial.print("Fan ");
      Serial.print(fans[fan_idx].name);
      Serial.print("(PIN D");
      Serial.print(fans[fan_idx].pin);
      Serial.print("): ");
      Serial.println(fans[fan_idx].speed);
    #endif
	}
}

void error() {
	//light up LED here
	digitalWrite(ERROR_LED_PIN, HIGH);
}

void initTempSensors() {
	// Start up the library
	sensorBus.begin();

	#if LOG_LEVEL >= LOG_INFO
		// locate devices on the bus
		Serial.print("Locating temperature sensors...");
		Serial.print("Found ");
		Serial.print(sensorBus.getDeviceCount(), DEC);
		Serial.println(" sensors.");
	#endif

	#if LOG_LEVEL >= LOG_DEBUG
		// report parasite power requirements
		Serial.print("Parasite power is: ");
		if (sensorBus.isParasitePowerMode()) Serial.println("ON");
		else Serial.println("OFF");
	#endif

	// set the resolution per device
	for (int i = 0; i < NUMBER_SENSORS; i++) {
		sensorBus.setResolution(tempSensors[i].address, TEMPERATURE_PRECISION);
	}
}

void initPwm() {
	#if LOG_LEVEL >= LOG_DEBUG
		// locate devices on the bus
		Serial.print("Initializing PWM...");
	#endif
	// modify PWM regeister to change PWM freq
	// set PWM freq to 31kHz (31372.55 Hz) on ...
	TCCR3B = TCCR3B & B11111000 | B00000001; // ... Pins D2  D3  D5
	TCCR4B = TCCR4B & B11111000 | B00000001; // ... Pins D6  D7  D8
	TCCR5B = TCCR5B & B11111000 | B00000001; // ... Pins D44 D45 D46

	for (int fan_idx = 0; fan_idx < NUMBER_FANS; fan_idx++)
		pinMode(fans[fan_idx].pin, OUTPUT);

	#if LOG_LEVEL >= LOG_DEBUG
		Serial.println("done");
	#endif
}

void setup()
{
	// start serial port
	#if LOG_LEVEL > 0
		Serial.begin(9600);
	#endif

	#if LOG_LEVEL >= LOG_DEBUG
		Serial.println("Fan Controller Starting...");
	#endif

	pinMode(ERROR_LED_PIN, OUTPUT);

	initTempSensors();

	initPwm();
}

void loop()
{
	int sensorReadingRetries = 0;
	while (readTemperatures() && sensorReadingRetries < MAX_BAD_SENSOR_READINGS) {
		//reading temperatures has failed - retry
		sensorReadingRetries++;
		#if LOG_LEVEL >= LOG_WARN
			Serial.println("Retrying Temperature reading.");
		#endif
		delay(1000);
	}

	if (sensorReadingRetries < MAX_BAD_SENSOR_READINGS)
		setStage();
	else {
		currentStage = DEFAULT_STAGE;
		#if LOG_LEVEL >= LOG_WARN
			Serial.println("Setting DEFAULT stage! Cause: too many bad sensor readings!");
		#endif
		error();
	}

	setFanSpeeds();

	// Update every 10 seconds
	delay(UPDATE_INTERVAL);
}
