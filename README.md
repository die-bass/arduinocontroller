# Arduino Fan Controller

## Overview
The Arduino fan controller is built for an ArdunoMega 2560.
There are 4 temperature sensors connected that control 4 fans.
The temperature sensors are DS18B20.
The fans are controller via fastPWM.

## Stage Setups:

### Stage 1 - Cold Start:
- Temperatures:
    - Amp Low < 30 C
    - Amp High < 30 C
    - Outlet < 30 C
- Fans:
    - Inlet: min%
    - Outlet: min%
    - Amp Angle: 0%
    - Amp Streight: 0%

### Stage 2 - Warm Start
- Temperatures:
    - Amp Low < 34.5 C
    - Amp High < 34.5 C
    - Outlet < 33.5 C
- Fans:
    - Inlet: 20%
    - Outlet: 20%
    - Amp Angle: 10%
    - Amp Streight: 10%

### Stage 3 - Running Cool
- Temperatures:
    - Amp Low < 50 C
    - Amp High < 50 C
    - Outlet < 35 C
- Fans:
    - Inlet: 40%
    - Outlet: 40%
    - Amp Angle: 20%
    - Amp Streight: 20%

### Stage 4 - Running Warm
- Temperatures:
    - Amp Low < 60 C
    - Amp High < 60 C
    - Outlet < 40 C
- Fans:
    - Inlet: 50%
    - Outlet: 50%
    - Amp Angle: 40%
    - Amp Streight: 40%

### Stage 5 - Running Hot
- Temperatures:
    - Amp Low < 75 C
    - Amp High < 75 C
    - Outlet < 45 C
- Fans:
    - Inlet: 80%
    - Outlet: 80%
    - Amp Angle: 50%
    - Amp Streight: 50%


### Default Stage - Running at Limit
- NOTE: this stage is not actually defined, as it gets selected if none of the above stages match
- Temperatures:
    - no condition
- Fans:
    - Inlet: 100%
    - Outlet: 100%
    - Amp1: 100%
    - Amp2: 100%
    - Amp3: 100%
